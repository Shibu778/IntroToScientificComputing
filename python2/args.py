#!/usr/bin/env python3

import argparse

def main():
    # First we initialize the parser. This is an object which will store
    # details of our arguments.
    parser = argparse.ArgumentParser()
    # Then we add each argument we want our code to support. There are several
    # options here we can specify, such as "help" which adds help text for the
    # argument. This will add a positional argument.
    parser.add_argument("datafile", help="The filename for the datafile.")
    # We can also add optional arguments and specify both long and short flags
    # along with the type and a default value.
    parser.add_argument("-t", "--threshold", type=float, default=1.0e-6,
            help="Values are considered zero below this threshold.")
    # Once we're finished, we parse the arguments passed to the code and
    # store them.
    args = parser.parse_args()

    print("The value of datafile is", args.datafile)
    print("The value of threshold is", args.threshold)

if __name__ == "__main__":
    main()
