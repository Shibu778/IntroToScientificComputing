#!/usr/bin/env python3

def hello():
    """Output the text 'Hello World!'."""
    print("Hello World!")

# We can call this function by entering its name.
hello()
hello()
