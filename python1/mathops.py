#!/usr/bin/env python3

import math as m
# This allows us to refer to functions as e.g. m.sin instead of math.sin

print(m.pi)
print(m.sin(m.pi / 2))
print(m.cos(m.pi / 2))
print(m.sin(0))
print(m.log(1.0))
