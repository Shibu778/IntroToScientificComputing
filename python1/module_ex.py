#!/usr/bin/env python3

import func_average2

def text_to_floats(line):
    """Convert a string containing numbers to a python list of floats."""
    # First convert any commas or tabs to spaces
    line = line.replace(',', ' ')
    line = line.replace('\t', ' ')
    line_vals = []
    # We need to initialize an empty list before we can add to it.
    for x in line.split():
        # The .split() method breaks up a string based on spaces by default.
        line_vals.append(float(x))
        # We convert each item to a float and append it to our list.
    return line_vals

while True:
  input_nums = input("Enter some numbers: ")
  # We keep looping until no text is entered.
  if input_nums == "":
      break
  num_list = text_to_floats(input_nums)
  print("The average of these is", func_average2.average(num_list))
  # The above calls the "average" function from the "func_average2" module.
